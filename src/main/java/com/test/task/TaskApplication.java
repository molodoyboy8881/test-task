package com.test.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.batch.BatchAutoConfiguration;


@SpringBootApplication(exclude={BatchAutoConfiguration.class})
public class TaskApplication {

    public static void main(String[] args) {
       SpringApplication.run(TaskApplication.class, args);
    }

}
