package com.test.task.exception;

public class NutritionScoreException extends RuntimeException {

    public NutritionScoreException(String message) {
        super(message);
    }
}
