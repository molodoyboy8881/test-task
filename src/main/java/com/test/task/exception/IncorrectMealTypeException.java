package com.test.task.exception;

public class IncorrectMealTypeException extends RuntimeException {

    public IncorrectMealTypeException(String message) {
        super(message);
    }
}
