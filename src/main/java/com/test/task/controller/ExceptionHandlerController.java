package com.test.task.controller;

import com.test.task.exception.IncorrectMealTypeException;
import com.test.task.exception.NutritionScoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlerController {
    private final static Logger logger =
            LoggerFactory.getLogger(ExceptionHandlerController.class.getName());

    @ExceptionHandler({IncorrectMealTypeException.class, NutritionScoreException.class})
    public ResponseEntity<?> incorrectInputDataExceptionHandler(RuntimeException e) {
        logger.warn(e.getMessage());

        return ResponseEntity.badRequest().build();
    }
}
