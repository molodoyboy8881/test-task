package com.test.task.controller;

import com.test.task.domain.Meal;
import com.test.task.domain.ConsumedMeals;
import com.test.task.domain.type.Day;
import com.test.task.domain.type.MealSpecification;
import com.test.task.exception.NutritionScoreException;
import com.test.task.service.MealTrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/meal_track")
public class MealTrackController {
    private final MealTrackService service;

    @Autowired
    public MealTrackController(MealTrackService service) {
        this.service = service;
    }

    @GetMapping("/{scheduleId}/{day}")
    public Map<MealSpecification, Set<Meal>> findMealBySpecification(@PathVariable Integer scheduleId,
                                                                     @PathVariable Day day) {
        return service.findMealBySpecification(scheduleId, day);
    }

    @GetMapping("/score/{scheduleId}/{day}")
    public float getNutritionScore(@PathVariable Integer scheduleId,
                                   @PathVariable Day day) throws NutritionScoreException {
        return service.getNutritionScore(scheduleId, day);
    }

    @PostMapping(value = "/", consumes = "application/json")
    public void addConsumedMeals(@RequestBody ConsumedMeals consumedMeals) {
        service.addConsumedMeals(consumedMeals);
    }

    @PutMapping(value = "/", consumes = "application/json")
    public boolean editConsumedMeals(@RequestBody ConsumedMeals consumedMeals) {
        return service.editConsumedMeals(consumedMeals);
    }
}
