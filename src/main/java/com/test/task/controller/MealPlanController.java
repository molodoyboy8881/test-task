package com.test.task.controller;

import com.test.task.domain.Meal;
import com.test.task.domain.MealPlan;
import com.test.task.domain.type.Day;
import com.test.task.exception.IncorrectMealTypeException;
import com.test.task.service.MealPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/meal_plan")
public class MealPlanController {
    private final MealPlanService service;

    @Autowired
    public MealPlanController(MealPlanService service) {
        this.service = service;
    }

    @GetMapping(value = "/{scheduleId}/{day}")
    public MealPlan findByScheduleIdOnDay(@PathVariable Integer scheduleId,
                                          @PathVariable Day day) {
        return service.findByScheduleIdOnDay(scheduleId, day);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<Boolean> save(@RequestBody MealPlan mealPlan) throws IncorrectMealTypeException {
        return ResponseEntity.ok().body(service.save(mealPlan));
    }

    @PutMapping(consumes = "application/json")
    public boolean setWorkoutPlan(@RequestBody Meal meal) throws IncorrectMealTypeException {
        return service.setWorkoutPlan(meal);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
}
