package com.test.task.controller;

import com.test.task.domain.Schedule;
import com.test.task.domain.type.Day;
import com.test.task.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/schedule")
public class ScheduleController {
    private final ScheduleService service;

    @Autowired
    public ScheduleController(ScheduleService service) {
        this.service = service;
    }

    @GetMapping("/{userId}/{day}")
    public Schedule findByUserIdOnDay(@PathVariable Integer userId,
                                      @PathVariable Day day) {
        return service.findByUserIdOnDay(userId, day);
    }

    @PostMapping(consumes = "application/json")
    public Schedule create(@RequestParam Integer userId,
                           @RequestParam String name) {
        return service.create(userId, name);
    }
}
