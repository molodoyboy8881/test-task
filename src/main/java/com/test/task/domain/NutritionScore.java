package com.test.task.domain;

import java.util.Set;

public class NutritionScore {
    private final Set<Meal> targetMeal;
    private final Set<Meal> consumedMeal;

    public NutritionScore(Set<Meal> targetMeal,
                          Set<Meal> consumedMeal) {
        this.targetMeal = targetMeal;
        this.consumedMeal = consumedMeal;
    }

    public float calculateScore() {
        float target = nutritionSumPerDay(targetMeal);
        float consumed = nutritionSumPerDay(consumedMeal);
        float d = calculateD(target, consumed);

        return Math.round(5 * Math.exp(-2.3 * d));
    }

    private float nutritionSumPerDay(Set<Meal> meals) {
        float sum = 0;
        for (Meal meal : meals) {
            sum += calculateNutritionSumPerMeal(meal.getNutrition());
        }

        return sum;
    }

    private float calculateNutritionSumPerMeal(Nutrition nutrition) {
        return nutrition.getCarbohydratesGrams() + nutrition.getFatsGrams() +
                nutrition.getProteinGrams();
    }

    private float calculateD(float target, float consumed) {
        return Math.abs(target - consumed) / target;
    }
}
