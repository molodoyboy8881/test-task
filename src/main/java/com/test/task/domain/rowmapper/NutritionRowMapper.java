package com.test.task.domain.rowmapper;

import com.test.task.domain.Nutrition;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NutritionRowMapper implements RowMapper<Nutrition> {

    @Override
    public Nutrition mapRow(ResultSet rs, int rowNum) throws SQLException {
        float fatsGrams = rs.getFloat("fats_grams");
        float proteinGrams = rs.getFloat("protein_grams");
        float carbohydrateGrams = rs.getFloat("carbohydrate_grams");

        return new Nutrition(fatsGrams, proteinGrams, carbohydrateGrams);
    }
}
