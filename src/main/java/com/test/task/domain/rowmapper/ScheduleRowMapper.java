package com.test.task.domain.rowmapper;

import com.test.task.domain.Schedule;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ScheduleRowMapper implements RowMapper<Schedule> {

    @Override
    public Schedule mapRow(ResultSet rs, int rowNum) throws SQLException {
        Integer id = rs.getInt("schedule_id");
        Integer user_id = rs.getInt("user_id");
        String name = rs.getString("schedule_name");

        return new Schedule(id, name, user_id);
    }
}
