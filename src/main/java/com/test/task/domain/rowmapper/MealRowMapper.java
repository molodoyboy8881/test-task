package com.test.task.domain.rowmapper;

import com.test.task.domain.Meal;
import com.test.task.domain.type.MealSpecification;
import com.test.task.domain.type.MealType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MealRowMapper implements RowMapper<Meal> {
    private final NutritionRowMapper nutritionRowMapper;

    public MealRowMapper(NutritionRowMapper nutritionRowMapper) {
        this.nutritionRowMapper = nutritionRowMapper;
    }

    @Override
    public Meal mapRow(ResultSet rs, int rowNum) throws SQLException {
        Integer id = rs.getInt("meal_id");
        Integer mealPlanId = rs.getInt("meal_plan_id");
        MealType mealType = MealType.valueOf(rs.getString("meal_type_name"));
        MealSpecification specification = MealSpecification.valueOf(rs.getString("meal_specification_name"));

        Meal meal = new Meal(mealType, nutritionRowMapper.mapRow(rs, rowNum), specification);
        meal.setId(id);
        meal.setMealPlanId(mealPlanId);

        return meal;
    }
}
