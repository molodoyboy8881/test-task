package com.test.task.domain.rowmapper;

import com.test.task.domain.MealPlan;
import com.test.task.domain.type.Day;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MealPlanRowMapper implements RowMapper<MealPlan> {

    @Override
    public MealPlan mapRow(ResultSet rs, int rowNum) throws SQLException {
        Day day = Day.valueOf(rs.getString("day_name"));
        String planName= rs.getString("plan_name");
        boolean workoutDay = rs.getBoolean("workout_day");
        Integer scheduleId = rs.getInt("schedule_id");

        MealPlan mealPlan = new MealPlan(day, planName, workoutDay, scheduleId);
        mealPlan.setId(rs.getInt("plan_id"));
        return mealPlan;
    }
}
