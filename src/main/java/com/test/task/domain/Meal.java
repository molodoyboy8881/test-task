package com.test.task.domain;

import com.test.task.domain.type.MealSpecification;
import com.test.task.domain.type.MealType;

import java.util.Objects;

public class Meal {
    private Integer id;
    private Integer mealPlanId;
    private MealType mealType;
    private Nutrition nutrition;
    private MealSpecification specification;

    public Meal(MealType mealType,
                Nutrition nutrition,
                MealSpecification specification) {
        this.mealType = mealType;
        this.nutrition = nutrition;
        this.specification = specification;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMealPlanId() {
        return mealPlanId;
    }

    public void setMealPlanId(Integer mealPlanId) {
        this.mealPlanId = mealPlanId;
    }

    public MealType getMealType() {
        return mealType;
    }

    public void setMealType(MealType mealType) {
        this.mealType = mealType;
    }

    public Nutrition getNutrition() {
        return nutrition;
    }

    public void setNutrition(Nutrition nutrition) {
        this.nutrition = nutrition;
    }

    public MealSpecification getMealSpecification() {
        return specification;
    }

    public void setMealSpecification(MealSpecification specification) {
        this.specification = specification;
    }

    @Override
    public String toString() {
        return "Meal{" +
                "id=" + id +
                ", mealPlanId=" + mealPlanId +
                ", mealType=" + mealType +
                ", nutrition=" + nutrition +
                ", specification=" + specification +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Meal meal = (Meal) o;
        return mealPlanId.equals(meal.mealPlanId) && mealType == meal.mealType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mealPlanId, mealType);
    }
}
