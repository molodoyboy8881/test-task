package com.test.task.domain;

import com.test.task.domain.type.Day;

import java.util.Set;

public class ConsumedMeals {
    private Day day;
    private Set<Meal> meals;
    private Integer scheduleId;

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Set<Meal> getMeals() {
        return meals;
    }

    public void setMeals(Set<Meal> meals) {
        this.meals = meals;
    }

    public Integer getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }
}
