package com.test.task.domain.type;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Locale;

public enum MealType {
    BREAKFAST(1),
    LUNCH(2),
    DINNER(3),
    WORKOUT_SNACK(4);

    private final int id;

    MealType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @JsonCreator
    public static MealType forValues(@JsonProperty("mealType") String mealType) {
        return MealType.valueOf(mealType.toUpperCase(Locale.ROOT));
    }

    @JsonValue
    public String toValue() {
        return this.name().toUpperCase(Locale.ROOT);
    }
}
