package com.test.task.domain.type;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Locale;

public enum Day {
    MONDAY(1),
    TUESDAY(2),
    WEDNESDAY(3),
    THURSDAY(4),
    FRIDAY(5),
    SATURDAY(6),
    SUNDAY(7);

    private final int id;

    Day(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @JsonCreator
    public static Day forValues(@JsonProperty("day") String day) {
        return Day.valueOf(day.toUpperCase(Locale.ROOT));
    }

    @JsonValue
    public String toValue() {
        return this.name().toUpperCase(Locale.ROOT);
    }
}
