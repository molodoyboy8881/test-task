package com.test.task.domain.type;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Locale;

public enum MealSpecification {
    TARGET(1),
    CONSUMED(2);

    private final int id;

    MealSpecification(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @JsonCreator
    public static MealSpecification forValues(@JsonProperty("mealSpecification") String mealSpecification) {
        return MealSpecification.valueOf(mealSpecification.toUpperCase(Locale.ROOT));
    }

    @JsonValue
    public String toValue() {
        return this.name().toUpperCase(Locale.ROOT);
    }
}
