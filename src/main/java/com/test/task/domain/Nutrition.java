package com.test.task.domain;

public class Nutrition {
    private float fatsGrams;
    private float proteinGrams;
    private float carbohydratesGrams;

    public Nutrition(float fatsGrams,
                     float proteinGrams,
                     float carbohydratesGrams) {
        this.fatsGrams = fatsGrams;
        this.proteinGrams = proteinGrams;
        this.carbohydratesGrams = carbohydratesGrams;

    }

    public float getFatsGrams() {
        return fatsGrams;
    }

    public void setFatsGrams(float fatsGrams) {
        this.fatsGrams = fatsGrams;
    }

    public float getProteinGrams() {
        return proteinGrams;
    }

    public void setProteinGrams(float proteinGrams) {
        this.proteinGrams = proteinGrams;
    }

    public float getCarbohydratesGrams() {
        return carbohydratesGrams;
    }

    public void setCarbohydratesGrams(float carbohydratesGrams) {
        this.carbohydratesGrams = carbohydratesGrams;
    }

    @Override
    public String toString() {
        return "Nutrition{" +
                "fatsGrams=" + fatsGrams +
                ", proteinGrams=" + proteinGrams +
                ", carbohydratesGrams=" + carbohydratesGrams +
                '}';
    }
}
