package com.test.task.domain;

import com.test.task.domain.type.Day;

import java.util.Objects;
import java.util.Set;

public class MealPlan {
    private Integer id;
    private Day day;
    private String planName;
    private boolean workoutDay;
    private Integer scheduleId;
    private Set<Meal> meals;

    public MealPlan(Day day,
                    String planName,
                    boolean workoutDay,
                    Integer scheduleId) {
        this.day = day;
        this.planName = planName;
        this.workoutDay = workoutDay;
        this.scheduleId = scheduleId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public boolean isWorkoutDay() {
        return workoutDay;
    }

    public void setWorkoutDay(boolean workoutDay) {
        this.workoutDay = workoutDay;
    }

    public Integer getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Set<Meal> getMeals() {
        return meals;
    }

    public void setMeals(Set<Meal> meals) {
        this.meals = meals;
    }

    @Override
    public String toString() {
        return "MealPlan{" +
                "id=" + id +
                ", day=" + day +
                ", planName='" + planName + '\'' +
                ", workoutDay=" + workoutDay +
                ", meals=" + meals +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MealPlan mealPlan = (MealPlan) o;
        return workoutDay == mealPlan.workoutDay && day == mealPlan.day && scheduleId.equals(mealPlan.scheduleId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(day, workoutDay, scheduleId);
    }
}
