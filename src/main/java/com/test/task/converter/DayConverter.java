package com.test.task.converter;

import com.test.task.domain.type.Day;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;

public class DayConverter implements Converter<String, Day> {

    @Override
    public Day convert(@NonNull String source) {
        return Day.valueOf(source.toUpperCase());
    }
}