package com.test.task.service;

import com.test.task.domain.MealPlan;
import com.test.task.domain.Schedule;
import com.test.task.domain.type.Day;
import com.test.task.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
public class ScheduleService {
    private final MealPlanService mealPlanService;
    private final ScheduleRepository scheduleRepository;

    @Autowired
    public ScheduleService(MealPlanService mealPlanService,
                           ScheduleRepository scheduleRepository) {
        this.mealPlanService = mealPlanService;
        this.scheduleRepository = scheduleRepository;
    }

    @Transactional
    public Schedule findByUserIdOnDay(Integer userId, Day day) {
        Schedule schedule = scheduleRepository.findByUserId(userId);
        MealPlan mealPlans = mealPlanService.findByScheduleIdOnDay(schedule.getId(), day);
        schedule.setMealPlans(Set.of(mealPlans));

        return schedule;
    }

    public Schedule create(Integer userId, String name) {
        return scheduleRepository.create(userId, name);
    }

    public void defineMealPlan(Schedule schedule, String name, Day day, boolean workout) {
        MealPlan mealPlan = new MealPlan(day, name, workout, schedule.getId());
        mealPlanService.save(mealPlan);
    }
}
