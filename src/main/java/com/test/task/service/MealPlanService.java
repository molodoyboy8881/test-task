package com.test.task.service;

import com.test.task.domain.Meal;
import com.test.task.domain.MealPlan;
import com.test.task.domain.type.Day;
import com.test.task.exception.IncorrectMealTypeException;
import com.test.task.repository.MealPlanRepository;
import com.test.task.repository.MealRepository;
import com.test.task.service.validation.WorkoutValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
public class MealPlanService {
    private final MealRepository mealRepository;
    private final MealPlanRepository mealPlanRepository;
    private final WorkoutValidation workoutValidation;

    @Autowired
    public MealPlanService(MealRepository mealRepository,
                           MealPlanRepository mealPlanRepository,
                           WorkoutValidation workoutValidation) {
        this.mealRepository = mealRepository;
        this.mealPlanRepository = mealPlanRepository;
        this.workoutValidation = workoutValidation;
    }

    @Transactional
    public MealPlan findByScheduleIdOnDay(Integer scheduleId, Day day) {
        MealPlan mealPlan = mealPlanRepository
                .findByScheduleIdOnDay(scheduleId, day);
        Set<Meal> meals = mealRepository.findTargetByMealPlan(mealPlan.getId());
        mealPlan.setMeals(meals);

        return mealPlan;
    }

    @Transactional
    public boolean save(MealPlan mealPlan) throws IncorrectMealTypeException {
        workoutValidation.validate(mealPlan);
        boolean exists = mealPlanRepository.exists(mealPlan);
        if (exists) {
            return false;
        }

        mealPlanRepository.save(mealPlan);

        setMealPlanIdToMeals(mealPlan);
        mealRepository.save(mealPlan.getMeals());

        return true;
    }

    private void setMealPlanIdToMeals(MealPlan mealPlan) {
        for (Meal meal : mealPlan.getMeals()) {
            meal.setMealPlanId(mealPlan.getId());
        }
    }

    @Transactional
    public boolean setWorkoutPlan(Meal meal) throws IncorrectMealTypeException {
        workoutValidation.validate(meal);
        boolean updated = mealPlanRepository
                .updateWorkoutDay(meal.getMealPlanId(), true);
        if (!updated) {
            return false;
        }

        mealRepository.save(meal);

        return true;
    }

    public void delete(Integer id) {
        mealPlanRepository.delete(id);
    }
}
