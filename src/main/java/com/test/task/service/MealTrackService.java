package com.test.task.service;

import com.test.task.domain.Meal;
import com.test.task.domain.NutritionScore;
import com.test.task.domain.ConsumedMeals;
import com.test.task.domain.type.Day;
import com.test.task.domain.type.MealSpecification;
import com.test.task.exception.NutritionScoreException;
import com.test.task.repository.MealTrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MealTrackService {
    private final MealTrackRepository mealTrackRepository;

    @Autowired
    public MealTrackService(MealTrackRepository mealTrackRepository) {
        this.mealTrackRepository = mealTrackRepository;
    }

    public Map<MealSpecification, Set<Meal>> findMealBySpecification(Integer scheduleId, Day day) {
        return mealTrackRepository.findAllByDay(scheduleId, day)
                .stream()
                .map(meal -> Map.entry(meal.getMealSpecification(), meal))
                .collect(Collectors.groupingBy(Map.Entry::getKey, Collectors.mapping(Map.Entry::getValue, Collectors.toSet())));
    }

    public float getNutritionScore(Integer scheduleId, Day day) throws NutritionScoreException {
        Map<MealSpecification, Set<Meal>> nutritionByType = findMealBySpecification(scheduleId, day);
        if (nutritionByType.size() != MealSpecification.values().length) {
            throw new NutritionScoreException("Invalid input data!");
        }

        return new NutritionScore(nutritionByType.get(MealSpecification.TARGET),
                nutritionByType.get(MealSpecification.CONSUMED)).calculateScore();
    }

    public void addConsumedMeals(ConsumedMeals consumedMeals) {
        mealTrackRepository.saveConsumed(consumedMeals);
    }

    public boolean editConsumedMeals(ConsumedMeals consumedMeals) {
        return mealTrackRepository.updateConsumedMeals(consumedMeals);
    }
}
