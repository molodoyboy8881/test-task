package com.test.task.service.validation;

import com.test.task.domain.Meal;
import com.test.task.domain.MealPlan;
import com.test.task.domain.type.MealType;
import com.test.task.exception.IncorrectMealTypeException;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class WorkoutValidation {

    public void validate(Meal meal) throws IncorrectMealTypeException {
        if (!meal.getMealType().equals(MealType.WORKOUT_SNACK)) {
            throw new IncorrectMealTypeException("Meal type must be workout!");
        }
    }

    public void validate(MealPlan mealPlan) throws IncorrectMealTypeException {
        Set<MealType> mealTypes = convertMealsIntoMealTypes(mealPlan.getMeals());
        boolean containsWorkoutSnack = mealTypes.contains(MealType.WORKOUT_SNACK);
        if (mealPlan.isWorkoutDay() ^ containsWorkoutSnack) {
            throw new IncorrectMealTypeException(buildMessage(mealPlan));
        }
    }

    private Set<MealType> convertMealsIntoMealTypes(Set<Meal> meals) {
        return meals
                .stream()
                .map(Meal::getMealType)
                .collect(Collectors.toSet());
    }

    private String buildMessage(MealPlan mealPlan) {
        if (mealPlan.isWorkoutDay()) {
            return "Meal plan " + mealPlan.getPlanName() + " should contained workout snack!";
        }

        return "Meal plan " + mealPlan.getPlanName() + " should not contained workout snack!";
    }
}
