package com.test.task.repository;

import com.test.task.domain.Schedule;
import com.test.task.domain.rowmapper.ScheduleRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class ScheduleRepository {
    private final ScheduleRowMapper scheduleRowMapper;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public ScheduleRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.scheduleRowMapper = new ScheduleRowMapper();
    }

    public Schedule findByUserId(Integer userId) {
        String sql = "SELECT * " +
                        "FROM schedule " +
                        "WHERE user_id = :user_id";

        return jdbcTemplate.queryForObject(sql, Map.of("user_id", userId) ,scheduleRowMapper);
    }

    public Schedule create(Integer userId, String name) {
        String sql = "INSERT INTO schedule(user_id, schedule_name) VALUES (:user_id, :schedule_name) " +
                "ON CONFLICT (user_id) DO UPDATE SET schedule_name = :schedule_name RETURNING schedule_id;";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, map(name, userId), keyHolder);

        return new Schedule(keyHolder.getKeyAs(Integer.class), name, userId);
    }

    private SqlParameterSource map(String name, Integer userId) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("user_id", userId);
        source.addValue("schedule_name", name);

        return source;
    }
}
