package com.test.task.repository;

import com.test.task.domain.Meal;
import com.test.task.domain.MealPlan;
import com.test.task.domain.rowmapper.MealRowMapper;
import com.test.task.domain.rowmapper.NutritionRowMapper;
import com.test.task.repository.parametermapper.MealParameterMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class MealRepository {
    private final MealRowMapper mealRowMapper;
    private final MealParameterMapper mealMapper;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public MealRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.mealMapper = new MealParameterMapper();
        this.mealRowMapper = new MealRowMapper(new NutritionRowMapper());
    }

    public Set<Meal> findTargetByMealPlan(Integer mealPlanId) {
        String sql = "SELECT * FROM meal m " +
                "JOIN meal_specification ms ON m.meal_specification_id = ms.id " +
                "JOIN nutrition n ON m.meal_id = n.meal_id " +
                "JOIN meal_type mt ON m.meal_type_id = mt.id " +
                "WHERE m.meal_plan_id = :meal_plan_id AND m.meal_specification_id = :target";

        return new HashSet<>(jdbcTemplate.query(sql,
                Map.of("meal_plan_id", mealPlanId, "target", 1), mealRowMapper));
    }

    public void save(Meal meal) {
        save(Set.of(meal));
    }

    public void save(Set<Meal> meals) {
        String sql = "WITH cte_meal(id) AS ( " +
                        "INSERT INTO meal(meal_plan_id, meal_type_id, meal_specification_id)" +
                        "VALUES (:meal_plan_id, :meal_type_id, :meal_specification_id) " +
                        "RETURNING meal_id" +
                     ") " +
                     "INSERT INTO nutrition(fats_grams, protein_grams, carbohydrate_grams, meal_id) " +
                        "VALUES (:fats_grams, :protein_grams, :carbohydrate_grams, (SELECT id FROM cte_meal));";

        jdbcTemplate.batchUpdate(sql, mealMapper.mapMeal(meals));
    }
}
