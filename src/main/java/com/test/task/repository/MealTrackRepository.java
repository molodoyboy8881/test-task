package com.test.task.repository;

import com.test.task.domain.Meal;
import com.test.task.domain.rowmapper.MealRowMapper;
import com.test.task.domain.rowmapper.NutritionRowMapper;
import com.test.task.domain.ConsumedMeals;
import com.test.task.domain.type.Day;
import com.test.task.repository.parametermapper.MealParameterMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class MealTrackRepository {
    private final MealRowMapper mealRowMapper;
    private final MealParameterMapper mealMapper;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public MealTrackRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.mealMapper = new MealParameterMapper();
        this.mealRowMapper = new MealRowMapper(new NutritionRowMapper());
    }

    public List<Meal> findAllByDay(Integer scheduleId, Day day) {
        String sql = "WITH cte(id) AS (" +
                        "SELECT plan_id " +
                        "FROM meal_plan " +
                        "WHERE day_id = :day_id AND schedule_id = :schedule_id " +
                    ")" +
                "SELECT * FROM meal m " +
                    "JOIN meal_specification ms ON m.meal_specification_id = ms.id " +
                    "JOIN meal_type mt ON m.meal_type_id = mt.id " +
                    "JOIN nutrition n ON m.meal_id = n.meal_id " +
                    "WHERE meal_plan_id = (SELECT id FROM cte);";

        return jdbcTemplate.query(sql, Map.of("day_id", day.getId(),
                "schedule_id", scheduleId), mealRowMapper);
    }

    public void saveConsumed(ConsumedMeals consumedMeals) {
        String sql = "WITH cte_plan AS (" +
                        "SELECT plan_id " +
                        "FROM meal_plan " +
                        "WHERE schedule_id = :schedule_id AND day_id = :day_id" +
                    "), " +
                    "cte_meal(id) AS ( " +
                        "INSERT INTO meal(meal_plan_id, meal_type_id, meal_specification_id) " +
                        "VALUES ((SELECT plan_id FROM cte_plan), :meal_type_id, :meal_specification_id) " +
                        "RETURNING meal_id" +
                    ") " +
                "INSERT INTO nutrition(fats_grams, protein_grams, carbohydrate_grams, meal_id) " +
                    "VALUES (:fats_grams, :protein_grams, :carbohydrate_grams, (SELECT id FROM cte_meal));";

        jdbcTemplate.batchUpdate(sql, mealMapper.consumedMap(consumedMeals));
    }

    public boolean updateConsumedMeals(ConsumedMeals consumedMeals) {
        String sql = "UPDATE nutrition n " +
                        "SET fats_grams = (fats_grams + :fats_grams), " +
                            "protein_grams = (protein_grams + :protein_grams), " +
                            "carbohydrate_grams = (carbohydrate_grams + :carbohydrate_grams) " +
                        "WHERE meal_id = (SELECT meal.meal_id " +
                        "FROM meal " +
                        "WHERE meal_plan_id = (SELECT plan_id " +
                                                "FROM meal_plan " +
                                                "WHERE day_id = :day_id AND schedule_id = :schedule_id)  " +
                        "AND meal_type_id = :meal_type_id AND meal_specification_id = :meal_specification_id);";

        return jdbcTemplate.batchUpdate(sql,
                mealMapper.consumedMap(consumedMeals)).length == consumedMeals.getMeals().size();
    }
}
