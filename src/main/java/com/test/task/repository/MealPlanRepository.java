package com.test.task.repository;

import com.test.task.domain.MealPlan;
import com.test.task.domain.rowmapper.MealPlanRowMapper;
import com.test.task.domain.type.Day;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class MealPlanRepository {
    private final MealPlanRowMapper mealPlanRowMapper;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public MealPlanRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.mealPlanRowMapper = new MealPlanRowMapper();
    }

    public MealPlan findByScheduleIdOnDay(Integer scheduleId, Day day) {
        String sql = "SELECT * " +
                "FROM meal_plan " +
                "JOIN days d on d.id = meal_plan.day_id " +
                "WHERE day_id = :day_id AND schedule_id = :schedule_id;";

        return jdbcTemplate.queryForObject(sql,
                Map.of("schedule_id",
                        scheduleId, "day_id", day.getId()), mealPlanRowMapper);
    }

    public void save(MealPlan mealPlan) {
        String sql = "INSERT INTO meal_plan(plan_name, workout_day, day_id, schedule_id) " +
                "VALUES (:plan_name, :workout_day, :day_id, :schedule_id) RETURNING plan_id";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, map(mealPlan), keyHolder);

        mealPlan.setId(keyHolder.getKeyAs(Integer.class));
    }

    private SqlParameterSource map(MealPlan mealPlan) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("plan_name", mealPlan.getPlanName());
        source.addValue("workout_day", mealPlan.isWorkoutDay());
        source.addValue("day_id", mealPlan.getDay().getId());
        source.addValue("schedule_id", mealPlan.getScheduleId());

        return source;
    }

    @SuppressWarnings("ConstantConditions")
    public boolean exists(MealPlan mealPlan) {
        String sql = "SELECT EXISTS(SELECT * FROM meal_plan WHERE schedule_id = :schedule_id AND day_id = :day_id)";

        return jdbcTemplate.queryForObject(sql,
                Map.of("schedule_id", mealPlan.getScheduleId(),
                        "day_id", mealPlan.getDay().getId()), boolean.class);
    }

    public boolean updateWorkoutDay(Integer planId, boolean workoutDay) {
        String sql = "UPDATE meal_plan " +
                "SET workout_day = :workout_day " +
                "WHERE plan_id = :plan_id";

        return jdbcTemplate
                .update(sql, Map.of("workout_day", workoutDay, "plan_id", planId)) == 1;
    }

    public void delete(Integer planId) {
        String sql = "DELETE FROM meal_plan WHERE plan_id = :plan_id";

        jdbcTemplate.update(sql, Map.of("plan_id",  planId));
    }
}
