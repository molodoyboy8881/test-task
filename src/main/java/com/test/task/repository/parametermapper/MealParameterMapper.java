package com.test.task.repository.parametermapper;

import com.test.task.domain.Meal;
import com.test.task.domain.Nutrition;
import com.test.task.domain.ConsumedMeals;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.util.Iterator;
import java.util.Set;

public class MealParameterMapper {

    public SqlParameterSource[] mapMeal(Set<Meal> meals) {
        Iterator<Meal> iterator = meals.iterator();
        MapSqlParameterSource[] sources = new MapSqlParameterSource[meals.size()];
        for (int i = 0; i < sources.length; ++i) {
            Meal meal = iterator.next();
            sources[i] = new MapSqlParameterSource();
            mapMeal(meal, sources[i]);
        }

        return sources;
    }

    public SqlParameterSource[] consumedMap(ConsumedMeals consumedMeals) {
        Set<Meal> meals = consumedMeals.getMeals();
        Iterator<Meal> iterator = meals.iterator();
        MapSqlParameterSource[] sources = new MapSqlParameterSource[meals.size()];
        for (int i = 0; i < sources.length; ++i) {
            Meal meal = iterator.next();
            sources[i] = new MapSqlParameterSource();
            mapMeal(meal, sources[i]);

            sources[i].addValue("day_id", consumedMeals.getDay().getId());
            sources[i].addValue("schedule_id", consumedMeals.getScheduleId());
        }

        return sources;
    }

    public void mapMeal(Meal meal, MapSqlParameterSource source) {
        source.addValue("meal_plan_id", meal.getMealPlanId());
        source.addValue("meal_type_id", meal.getMealType().getId());
        source.addValue("meal_specification_id", meal.getMealSpecification().getId());

        mapNutrition(meal.getNutrition(), source);
    }

    private void mapNutrition(Nutrition nutrition, MapSqlParameterSource source) {
        source.addValue("fats_grams", nutrition.getFatsGrams());
        source.addValue("protein_grams", nutrition.getProteinGrams());
        source.addValue("carbohydrate_grams", nutrition.getCarbohydratesGrams());
    }
}
