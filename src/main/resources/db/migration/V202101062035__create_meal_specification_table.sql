CREATE TABLE meal_specification
(
    id SERIAL PRIMARY KEY,
    meal_specification_name VARCHAR(255)
);

INSERT INTO meal_specification(meal_specification_name) VALUES ('TARGET');
INSERT INTO meal_specification(meal_specification_name) VALUES ('CONSUMED');