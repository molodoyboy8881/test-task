CREATE TABLE meal
(
    meal_id      SERIAL PRIMARY KEY,
    meal_plan_id INTEGER REFERENCES meal_plan (plan_id) ON DELETE CASCADE,
    meal_type_id INTEGER REFERENCES meal_type (id) NOT NULL,
    meal_specification_id INTEGER REFERENCES meal_specification(id)
)