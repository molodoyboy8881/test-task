CREATE TABLE meal_type
(
    id SERIAL PRIMARY KEY,
    meal_type_name VARCHAR(255)
);

INSERT INTO meal_type(meal_type_name) VALUES ('BREAKFAST');
INSERT INTO meal_type(meal_type_name) VALUES ('LUNCH');
INSERT INTO meal_type(meal_type_name) VALUES ('DINNER');
INSERT INTO meal_type(meal_type_name) VALUES ('WORKOUT_SNACK');
