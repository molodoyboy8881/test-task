CREATE TABLE days
(
    id SERIAL PRIMARY KEY,
    day_name VARCHAR(255) NOT NULL
);

INSERT INTO days(day_name) VALUES('MONDAY');
INSERT INTO days(day_name) VALUES('TUESDAY');
INSERT INTO days(day_name) VALUES('WEDNESDAY');
INSERT INTO days(day_name) VALUES('THURSDAY');
INSERT INTO days(day_name) VALUES('FRIDAY');
INSERT INTO days(day_name) VALUES('SATURDAY');
INSERT INTO days(day_name) VALUES('SUNDAY');