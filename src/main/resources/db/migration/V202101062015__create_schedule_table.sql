CREATE TABLE schedule
(
  schedule_id SERIAL PRIMARY KEY,
  user_id INTEGER REFERENCES users(user_id) UNIQUE NOT NULL,
  schedule_name VARCHAR(255)
);