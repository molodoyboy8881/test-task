CREATE TABLE meal_plan
(
    plan_id     SERIAL PRIMARY KEY,
    plan_name   VARCHAR(255)                 NOT NULL,
    workout_day BOOLEAN                      NOT NULL,
    day_id      INTEGER REFERENCES days (id) NOT NULL,
    schedule_id INTEGER REFERENCES schedule(schedule_id) ON DELETE CASCADE
);