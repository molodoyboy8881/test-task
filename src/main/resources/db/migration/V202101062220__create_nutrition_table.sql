CREATE TABLE nutrition
(
    fats_grams         FLOAT                                  NOT NULL,
    protein_grams      FLOAT                                  NOT NULL,
    carbohydrate_grams FLOAT                                  NOT NULL,
    meal_id            INTEGER REFERENCES meal (meal_id) ON DELETE CASCADE
);